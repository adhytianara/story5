from django.urls import path

from .views import beranda, riwayat, skill, tentangsaya, formulirViews, delete
from django.conf.urls import url

urlpatterns = [
    url(r'^$', beranda, name='beranda'),
    url('riwayat/', riwayat, name='riwayat'),
    url('skill/', skill, name='skill'),
    url('tentang-saya/', tentangsaya, name='tentang-saya'),
    url('formulir/', formulirViews, name='formulir'),
    path('delete/<row_id>', delete, name = 'delete')
]