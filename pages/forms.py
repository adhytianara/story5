from django import forms
from .models import formulirModels
from django.utils import timezone
from datetime import datetime

class formulirForms(forms.Form):
    namaKegiatan = forms.CharField(label='Kegiatan', max_length=20,
        widget=forms.TextInput(attrs={'class' : 'form-control marginbottom10', 'placeholder' : 'nama kegiatan'}))
    
    tempat = forms.CharField(label='Tempat', max_length=10,
        widget=forms.TextInput(attrs={'class' : 'form-control marginbottom10', 'placeholder' : 'tempat kegiatan'}))
    
    kategori = forms.CharField(label='Kategori', max_length=10,
        widget=forms.TextInput(attrs={'class' : 'form-control marginbottom10', 'placeholder' : 'kategori'}))
    
    date_field = forms.DateField(label = 'Tanggal',
        widget=forms.DateInput(attrs={'type' : 'date' ,'class' : 'form-control marginbottom10', 'placeholder' : 'tanggal kegiatan'}))
    
    time_field = forms.TimeField(label = 'Waktu',
        widget=forms.TimeInput(attrs={'type' : 'time' ,'class' : 'form-control', 'placeholder' : 'waktu kegiatan'}))

