# Generated by Django 2.2.5 on 2019-10-04 22:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0002_remove_formulirmodels_waktu'),
    ]

    operations = [
        migrations.AlterField(
            model_name='formulirmodels',
            name='namaKegiatan',
            field=models.CharField(max_length=30, unique=True),
        ),
    ]
