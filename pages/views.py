from django.views.generic import TemplateView
from django.shortcuts import render, redirect

from .forms import formulirForms
from .models import formulirModels
from django.http import HttpResponseRedirect


def formulirViews(request):
    dictio={}
    data = formulirModels.objects.all()
    form = formulirForms()

    # hanya akan diakses jika pengguna menekan tombol "submit" pada form
    if request.method == 'POST':
        form = formulirForms(request.POST or None)
        dictio['form'] = form
        if form.is_valid():
            model = formulirModels(namaKegiatan = form['namaKegiatan'].value(), tempat = form['tempat'].value(), 
            kategori = form['kategori'].value(), date_field = form['date_field'].value(), time_field = form['time_field'].value())
            
            model.save()
            dictio['data'] = data
            return render(request, 'formulir.html', dictio)

    # diakses saat pengguna pertama kali masuk ke bagian jadwal dan database tidak kosong
    elif data.count != 0:
        return render(request, 'formulir.html', {'form': form, 'data': data})
    
    # hanya menampilkan template formulir
    return render(request, 'formulir.html', {'form': form})

# method delete akan menghapus satu baris data sesuai tombol yang ditekan
def delete(request, row_id):
    data = formulirModels.objects.get(pk=row_id)
    data.delete()
    return redirect('formulir')

def riwayat(request):
    return render(request, 'riwayat.html')

def skill(request):
    return render(request, 'skill.html')

def tentangsaya(request):
    return render(request, 'tentangsaya.html')

def beranda(request):
    return render(request, 'beranda.html')

