from django.db import models
from django.utils import timezone
from datetime import datetime

class formulirModels (models.Model):
    namaKegiatan = models.CharField(max_length = 20)
    tempat = models.CharField(max_length = 10)
    kategori = models.CharField(max_length = 10)
    date_field = models.DateField(default = timezone.now)
    time_field = models.TimeField(default = timezone.now)
